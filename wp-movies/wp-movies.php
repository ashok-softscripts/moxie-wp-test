<?php /*
  Plugin Name: WP Movies
  Description: Complete movies manager. Allow access movies data with JSON API. Displays movies easily in front-end.
  Version: 1.0
 */
global $wpdb;
$siteurl = get_bloginfo('url');
define('WPM_PLUGIN_URL', WP_PLUGIN_URL.'/wp-movies');

/* Load all functions */
require_once ( 'functions/functions.php' ); // Load Utility Functions
require_once ( 'admin/index.php' ); // Load Backend Pages
require_once ( 'api/api.php' ); // JSON API
require_once ( 'front-end/index.php' ); // Load Front end Pages


add_action('admin_menu','wpm_backend_menu');
function wpm_backend_menu() {
	add_submenu_page('edit.php?post_type=wp-movies','Settings','Settings','manage_options','wpm_settings','wpm_settings');
}

function wpm_json_api_init(){
	add_filter('rewrite_rules_array', 'wpm_json_api_rewrites');
}

add_action('init', 'wpm_json_api_init');

// this hook will cause our creation function to run when the plugin is activated
register_activation_hook( __FILE__, 'wpm_plugin_install' );

function wpm_plugin_install() {
	global $wp_rewrite;
  	add_filter('rewrite_rules_array', 'wpm_json_api_rewrites');
  	$wp_rewrite->flush_rules();
	
	global $wpdb; // do NOT forget this global
	update_option('disable_wpm_admin_message',1);

}

function wpm_admin_messages() {
	//If we're editing the events page show hello to new user
	$dismiss_link_joiner = ( count($_GET) > 0 ) ? '&amp;':'?';
	
	if( current_user_can('activate_plugins') ){
		//New User Intro
		if (isset ( $_GET ['disable_wpm_admin_message'] ) && $_GET ['disable_wpm_admin_message'] == 'true'){
			// Disable Hello to new user if requested
			update_option('disable_wpm_admin_message',0);
		}elseif ( get_option ( 'disable_wpm_admin_message' ) ) {
			
			$advice = sprintf( __("<p>WP Movies plugin is ready to go! Check out the <a href='%s'>Settings Page</a>. <a href='%s' title='Don't show this advice again'>Dismiss</a></p>", 'wpm'), wpm_get_url('settings'),  $_SERVER['REQUEST_URI'].$dismiss_link_joiner.'disable_wpm_admin_message=true');
			?>
			<div id="message" class="updated">
				<?php echo $advice; ?>
			</div>
			<?php
		}
	}
}
add_action ( 'admin_notices', 'wpm_admin_messages', 100 );


// Add settings link on plugin page
function wpm_settings_link($links) { 
  $forms_link = '<a href="'.wpm_get_url('movies').'">Movies</a>'; 
  $settings_link = '<a href="'.wpm_get_url('settings').'">Settings</a>'; 
  array_unshift($links, $forms_link); 
  array_unshift($links, $settings_link); 
  return $links; 
}
 
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'wpm_settings_link' );

// this hook will cause our creation function to run when the plugin is deactivated
register_deactivation_hook( __FILE__,'wpm_plugin_uninstall');
function wpm_plugin_uninstall() {
	global $wp_rewrite;
  	$wp_rewrite->flush_rules();
	
	global $wpdb; // do NOT forget this global
 	delete_option('disable_wpm_admin_message');
	delete_option('wpm_settings_options');
	
}

function wpm_json_api_rewrites($wp_rules) {
  $base = wpm_options('wpm_json_api_base');
  if (empty($base)) {
    return $wp_rules;
  }
  $wpm_json_api_rules = array(
    "$base\$" => 'index.php?wpm_json_api=1'
  );
  return array_merge($wpm_json_api_rules, $wp_rules);
}


?>