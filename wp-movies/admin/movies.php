<?php function load_wpm_post_type() { 
	register_post_type( 'wp-movies', 
	 	// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'WP Movies', 'wpm' ), /* This is the Title of the Group */
			'singular_name' => __( 'Movie', 'wpm' ), /* This is the individual type */
			'all_items' => __( 'All Movies', 'wpm' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'wpm' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Movie', 'wpm' ), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Movie', 'wpm' ), /* Edit Display Title */
			'new_item' => __( 'New Movie', 'wpm' ), /* New Display Title */
			'view_item' => __( 'View Movie', 'wpm' ), /* View Display Title */
			'search_items' => __( 'Search Movies', 'wpm' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'wpm' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'wpm' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the Movie post type', 'wpm' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 22, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-format-video', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'movie', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail' )
	 	) /* end of options */
	); /* end of register post type */    
} 

// adding the function to the Wordpress init
add_action( 'init', 'load_wpm_post_type');

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function wpm_add_meta_box() {
	add_meta_box('wpm_metaid', __( 'Meta Fields', 'wpm' ), 'wpm_meta_box_callback', 'wp-movies');	
}

add_action( 'add_meta_boxes', 'wpm_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current movie post type.
 */
function wpm_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'wpm_save_meta_box_data', 'wpm_meta_box_nonce' );

	$rating = get_post_meta( $post->ID, '_movie_rating', true );
	
	$year = get_post_meta( $post->ID, '_movie_year', true );
	
	echo '<table border="0">';
	echo '<tr>';
	echo '<td width="40%">';
	echo '<label for="wpm_rating">';
	_e( 'Rating', 'wpm' );
	echo '</label> ';
	echo '</td>';	
	echo '<td>';
	echo '<input type="number" id="wpm_rating" name="wpm_rating" value="' . esc_attr( $rating ) . '" size="25" />';
	echo '</td>';
	echo '</tr>';
	echo '<tr><td colspan="2">&nbsp;</td></tr>';	
	echo '<tr>';
	echo '<td>';
	echo '<label for="wpm_year">';
	_e( 'Year', 'wpm' );
	echo '</label> ';
	echo '</td>';	
	echo '<td>';
	echo '<input type="number" id="wpm_year" name="wpm_year" value="' . esc_attr( $year ) . '" size="25" />';
	echo '</tr>';
	echo '</table>';
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function wpm_save_meta_box_data( $post_id ) {

	// Check if our nonce is set.
	if ( ! isset( $_POST['wpm_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['wpm_meta_box_nonce'], 'wpm_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if (  isset( $_POST['wpm_rating'] ) ) {
		// Sanitize user inputs.
		$wpm_rating = sanitize_text_field( $_POST['wpm_rating'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_movie_rating', $wpm_rating );
	}
	
	if (  isset( $_POST['wpm_year'] ) ) {
		// Sanitize user inputs.
		$wpm_year = sanitize_text_field( $_POST['wpm_year'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_movie_year', $wpm_year );
	}

	
}
add_action( 'save_post', 'wpm_save_meta_box_data' );


//Change featured image to movie poster
function wpm_movies_image_box() {
	remove_meta_box('postimagediv', 'wp-movies', 'side');;
	add_meta_box('postimagediv', __('Movie Poster'), 'post_thumbnail_meta_box', 'wp-movies', 'side', 'low');

}
add_action('do_meta_boxes', 'wpm_movies_image_box');


// Display poster movie list page.
add_filter( 'manage_edit-wp-movies_columns', 'my_edit_wpm_columns' ) ;

function my_edit_wpm_columns( $columns ) {
	$columns['poster'] = __( 'Poster' );
	return $columns;
}

add_action( 'manage_wp-movies_posts_custom_column', 'my_manage_wpm_columns', 10, 2 );

function my_manage_wpm_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'poster' :

			/* Get the post meta. */
			$image = get_the_post_thumbnail($post_id,'thumbnail',array('style'=>'max-height:80px;width:auto'));

			/* If no poster is found, output a default place holder. */
			if ( empty( $image ) )
				echo '<img src="http://placehold.it/40x40" />';			
			else
				echo $image;
			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

?>