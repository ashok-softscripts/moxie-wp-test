<?php add_action( 'admin_init', 'wpm_settings_init' );

function wpm_settings_init(){
	register_setting( 'wpm_settings', 'wpm_settings_options', 'wpm_settings_validate' );
	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}

function wpm_settings() {
global $wpdb;
$link = wpm_get_url('settings');

if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;
 ?>
<div class="wrap wpm_wrap">
		<div class="add_new"><div id="icon-edit-pages" class="icon32"><br></div><h2>Settings</h2></div>
	<?php if ( false != $_REQUEST['settings-updated'] ) :  ?>
		<div class="updated"><p><strong><?php _e( 'Settings saved'); ?></strong></p></div>
		<?php endif; ?>
		<form method="post" action="options.php">
		<?php settings_fields( 'wpm_settings' ); 
		$settings = get_option( 'wpm_settings_options' ); ?>
			<table id="general-tab" class="form-table wpm_setting_table">
				<tr valign="top"><th scope="row"><?php _e( 'API base'); ?></th>
					<td>
						<div class="relative">
						<input type="text" id="wpm_settings_options_wpm_json_api_base" class="regular-text" name="wpm_settings_options[wpm_json_api_base]" value="<?php echo $settings['wpm_json_api_base']; ?>" />
						<p class="description" for="wpm_settings_options_wpm_json_api_base">Specify a base URL for WP Movies API. For example, using <code>api</code> as your API base URL would enable the following <code><?php echo site_url('api'); ?></code>. If you assign a blank value the API will only be available by setting a <code>wpm_json_api</code> query variable.</p>
						</div>				
					</td>
				</tr>								
		</table>

		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Save Settings'); ?>" />
		</p>
	</form>
</div>
<?php } 

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function wpm_settings_validate( $input ) {

	// Say our text option must be safe text with no HTML tags
	$input['entry_edit_page'] = wp_filter_nohtml_kses( $input['entry_edit_page'] );
	$input['wpm_json_api_base'] = wp_filter_nohtml_kses( $input['wpm_json_api_base'] );

	// Say our textarea option must be safe text with the allowed tags for posts
	//$input['textarea'] = wp_filter_post_kses( $input['textarea'] );

	return $input;
}
?>
