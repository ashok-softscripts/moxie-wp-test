<?php    
function wpm_filter_query_vars( $query_vars ) {
    $query_vars[] = 'wpm_json_api';

    return $query_vars;
}
add_filter( 'query_vars', 'wpm_filter_query_vars' );


function wpm_api_endpoint_data( $template ) {
    global $wp_query;

    // You could normally swap out the template WP wants to use here, but we'll just die
    if ( isset( $wp_query->query_vars['wpm_json_api'] ) && $wp_query->query_vars['wpm_json_api'] == 1 ) {
        $wp_query->is_404 = false;
        $wp_query->is_archive = true;
        $wp_query->is_category = true;
        $json_data = array();
     
        $args = array(
            'post_type'      => 'wp-movies',
            'posts_per_page' => -1
        );
        $wpm_query = new WP_Query( $args );
        if ( $wpm_query->have_posts() ) :
		$json_data['status'] = 'ok';
		$json_data['total_count'] = $wpm_query->found_posts;     
		while ( $wpm_query->have_posts() ) : $wpm_query->the_post();
			$post_id = get_the_ID();
            $img_id = get_post_thumbnail_id();
            $img = wp_get_attachment_image_src( $img_id, 'full' );
			$rating = get_post_meta( $post_id, '_movie_rating', true );
			$year = get_post_meta( $post_id, '_movie_year', true );
			$short_description = get_the_content();
            $json_data['data'][] = array(
				'id'	=> $post_id,
				'title' => get_the_title(),
                'poster_url'  => esc_url( $img[0] ), 
				'rating' => intval($rating),
				'year' => intval($year),
				'short_description' => $short_description
            );
        endwhile; wp_reset_postdata(); 
		else:
			$json_data['status'] = 'no movies found';
		endif;
		wp_send_json($json_data);
		
    } else {
        return $template;
    }
}
add_filter( 'template_include', 'wpm_api_endpoint_data' );
?>