<?php 
/***************************
 Get Plugin Backend URL
 :- Args: Data String
****************************/
function wpm_get_url($page) {
	if($page) {
		if($page == 'movies') {
			return get_admin_url(get_current_blog_id(), 'edit.php?post_type=wp-movies');
		}
		else {
			return get_admin_url(get_current_blog_id(),'admin.php?page=wpm_'.$page);
		}
	}
}

/********************************
 wpm Debug String/Array
 :- Args: Label, Value
*********************************/
function wpm_debug($label, $data) {
	echo "<div style=\"margin-left: 40px;background-color:#eeeeee;\"><u><h3>".$label."</h3></u><pre style=\"border-left:2px solid #000000;margin:10px;padding:4px;\">".print_r($data, true)."</pre></div>";
}


/********************************
 wpm Redirect String
 :- Args: Value
*********************************/
function wpm_redirect($url) {
	if($url) {
		echo "<script>location.href='".$url."';</script>";
	}
}

/********************************
 wpm Settings
 :- Args: Key
*********************************/
function wpm_options($key) {
$settings = get_option( 'wpm_settings_options' );
	if($key) {
		return $settings[$key];
	}
	else {
		return $settings;
	}
}


/********************************
 wpm Generate URL
 :- Args: Value

*********************************/
function wpm_url($atts = '') {
	$pageURL = get_permalink();
	if($atts) {
		if ( get_option('permalink_structure') ) {
			$pageURL = $pageURL.'?'.$atts;
		}
		else {
			$pageURL = $pageURL.'&'.$atts;
		}
	}
	return $pageURL;
}

?>