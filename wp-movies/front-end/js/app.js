var App = angular.module('MovieApp', []);

App.controller('MoviesCtrl', function($scope, $http) {
  $http.get(WPM.API)
       .then(function(res){
	      $scope.movies = res.data;                
        });	
   $scope.range = function(min, max, step){
		step = step || 1;
		var input = [];
		for (var i = min; i <= max; i += step) input.push(i);
		return input;
	  };
});
App.filter('unsafe', function($sce) {
		return function(val) {
			return $sce.trustAsHtml(val);
		};
	});
