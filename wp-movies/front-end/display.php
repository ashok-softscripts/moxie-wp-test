<?php function wpm_display_movies(){ ?>
	<div class="wpm-container" ng-app="MovieApp">
		<div ng-controller="MoviesCtrl">			
			<ul ng-if="movies.total_count > 0" class="wpm-list">
				<li ng-repeat="movie in movies.data" class="wpm-item" >
					<a href="" ng-click="wpmpopup=true" class="wpm-poster"><img src="{{movie.poster_url}}" alt="{{movie.title}}" />		
					<h3 class="wpm-title">{{movie.title}}</h3>
					<span class="wpm-view">View</span>					
					<div class="wpm-meta">
						<div class="wpm-rating">
							<span ng-class="{ 'active': n <= movie.rating }" data-ng-repeat="n in range(1,5)" >☆</span>	
						</div>	
						<div class="wpm-year">{{movie.year}}</div>					
					</div>	
					</a>							
					<div class="wpm-text-overlay" ng-class="{ 'active': wpmpopup == true }">
					<div class="wpm-text">
						<span class="wpm-close" ng-click="wpmpopup=false">X</span>
						<h3>{{movie.title}}</h3>
						<div ng-bind-html="movie.short_description | unsafe"></div>
					</div>
					</div>	
				</li>
			</ul>			
		</div>		
	</div>        
<?php } ?>