<?php
require_once ( 'display.php' ); // Display movies with AngularJS
//add_shortcode('wpm', 'wpm_form');

add_filter( 'the_content', 'wpm_content_filter', 20 );
/**
 * Add a custom shortcode to the ending of selected page.
 *
 * @uses is_page()
 */
function wpm_content_filter( $content ) {
	$edit_page_id = wpm_options('entry_edit_page');
    if ( is_home() || is_front_page() )
		wpm_display_movies();

    // Returns the content.
    return $content;
}

add_action('wp_enqueue_scripts', 'wpm_enqueue_scripts' );
function wpm_enqueue_scripts(){
  // first check that $hook_suffix is appropriate for your admin page 
	if ( is_home() || is_front_page() ) {
  		wp_enqueue_script('wpm-angular', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.min.js', array('jquery'), false, false);

		$wpm_data_array = array(
			'API' => site_url('?wpm_json_api=1')
		);
		wp_enqueue_script( 'wpm-js', WPM_PLUGIN_URL . '/front-end/js/app.js', array(), false, true );
  		wp_localize_script( 'wpm-js', 'WPM', $wpm_data_array );
		wp_enqueue_style( 'wpm-styles', WPM_PLUGIN_URL . '/front-end/css/style.css', array(), '', 'all' );  
	}
}

?>